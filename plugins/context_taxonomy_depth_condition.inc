<?php

/**
 * @file
 * context_taxonomy_depth class, extends the context_condition_node
 */

/**
 * The taxonomy depth as context condition
 */
class context_taxonomy_depth_condition extends context_condition {

  function condition_values() {
    $values = array();
    for ($i = 1; $i <= 10; $i++) {
      $values[$i] = $i;
    }
    return $values;
  }

  /**
   * Condition form.
   */
  //function condition_form($context) {
  //  $form = parent::condition_form($context);
  //  $form['#type'] = 'select';
  //  $form['#multiple'] = FALSE;
  //
  //  for ($i = 1; $i <= 10; $i++) {
  //    $opts[$i] = $i;
  //  }
  //
  //  $form['#options'] = array('Depth' => $opts);
  //  return $form;
  //}

  /**
   * Execute.
   */
  function execute($term, $op) {
    // get the current depth of the taxonomy term
    $depth = 1 + count(taxonomy_get_parents_all($term->tid));
    
    foreach ($this->get_contexts($depth) as $context) {
      $this->condition_met($context, $depth);
    }
  }
}
